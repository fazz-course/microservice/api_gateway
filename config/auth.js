const jwt = require('jsonwebtoken')

const auth = (req, res, next) => {
    const { authorization } = req.headers

    if (!authorization) {
        return res.status(401).send({ message: 'silahkan login' })
    }

    jwt.verify(authorization, process.env.JWT_KEYS, (err, decode) => {
        if (err) {
            return res.status(401).send({ message: err })
        } else {
            req.user = decode.username
            next()
        }
    })
}

module.exports = (app, routers) => {
    routers.forEach((r) => {
        if (r.auth) app.use(r.url, auth, (req, res, next) => next())
    })
}
