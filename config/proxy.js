const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = (server, routers) => {
    routers.forEach((r) => {
        server.use(
            r.url,
            createProxyMiddleware({
                ...r.proxy
            })
        )
    })
}
