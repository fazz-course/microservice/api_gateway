module.exports = [
    {
        url: '/api/auth',
        auth: false,
        proxy: {
            target: 'http://127.0.0.1:2022',
            changeOrigin: true,
            pathRewrite: {
                [`^/api/auth/signin`]: '/signin',
                [`^/api/auth/signup`]: '/signup',
                [`^/api/auth/verify`]: '//verify'
            }
        }
    },
    {
        url: '/api/users',
        auth: true,
        proxy: {
            target: 'http://127.0.0.1:2023',
            changeOrigin: true,
            pathRewrite: {
                [`^/api/users`]: ''
            }
        }
    },
    {
        url: '/api/message',
        auth: false,
        proxy: {
            target: 'http://127.0.0.1:2025',
            changeOrigin: true,
            pathRewrite: {
                [`^/api/message`]: ''
            }
        }
    },
    {
        url: '/api/products',
        auth: false,
        proxy: {
            target: 'http://127.0.0.1:2024',
            changeOrigin: true,
            pathRewrite: {
                [`^/api/products`]: ''
            }
        }
    }
]
