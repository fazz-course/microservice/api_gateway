const express = require('express')
const app = express()

const routes = require('./config/router')
const setupProxies = require('./config/proxy')
const setupAuth = require('./config/auth')
const { logger, loggerMiddleware } = require('./config/logger')
const debug = logger(module)

app.use(loggerMiddleware)
setupAuth(app, routes)
setupProxies(app, routes)

app.listen(2021, () => {
    debug.info('listening on port 2021')
})
